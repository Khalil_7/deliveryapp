export  class User {
    
    constructor( 
        public fullname: string, 
        public username: string,
        public email: string,
        public role: string[],
        public password?: string,
        public isActive?: boolean,
        public _id?: number,
        ) {}

}