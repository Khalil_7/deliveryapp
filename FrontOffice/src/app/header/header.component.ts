import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuth = false;

  constructor(private userService: UserService, private router: Router) { 
    
  }

  ngOnInit(): void {
    this.isAuth = this.userService.isLogedUser();
  }

  onSignOut () {
    this.userService.signOutUser();
    this.isAuth = false;
    this.router.navigate(['/auth','signin']);
  }

}
