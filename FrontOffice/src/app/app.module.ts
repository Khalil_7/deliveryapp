import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { UserService } from './services/user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { AuthGuard } from './services/guards/auth.guard';
import { MenuComponent } from './menu/menu.component';
import { DashbordComponent } from './menu/dashbord/dashbord.component';
import { NotFoundComponent } from './menu/not-found/not-found.component';
import { StockComponent } from './menu/stock/stock.component';
import { OrdersComponent } from './menu/orders/orders.component';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    DashbordComponent,
    HeaderComponent,
    NotFoundComponent,
    MenuComponent,
    StockComponent,
    OrdersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    UserService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
