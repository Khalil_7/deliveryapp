import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {

  public userInfo : User;
  constructor(userServices: UserService) {
    this.userInfo = userServices.getUserData().userInfo;
  }

  ngOnInit(): void {
  }

}
