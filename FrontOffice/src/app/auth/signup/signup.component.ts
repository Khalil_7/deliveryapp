import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {

  signUpForm: FormGroup;
  errorMessage: string;
  subscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.initForm();
  }
  
  initForm() {
    this.signUpForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[A-z]+[a-z]+[0-9]+/)]]
    });
  }

  onSubmit() {            
    const role = ['client'];  
    const formValue = this.signUpForm.value;
    const user = new User(
      formValue['fullname'], 
      formValue['username'], 
      formValue['email'],  
      role, 
      formValue['password']); 
    this.subscription = this.userService.signUpUser(user).subscribe(
      (response) => {
        if(response.token && response.user) {
          const user= response.user;
          const token= response.token;
          this.userService.saveUserData(user, token);
          this.router.navigate(['/dashbord']);
        } else {
          this.errorMessage = response.message;
        }
      },
      (error) => {
        this.errorMessage = error.error.message;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
