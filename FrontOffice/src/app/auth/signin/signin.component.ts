import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit , OnDestroy {
  signInForm: FormGroup;
  errorMessage: string;
  subscription: Subscription;
  constructor(
    private userService: UserService, 
    private router: Router,
    private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.signInForm = this.formBuilder.group({
      usernameOrEmail: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.pattern(/[A-z]+[a-z]+[0-9]+/)]]
    });
  }

  onSubmit() {
    const formValue = this.signInForm.value;
    const usernameOrEmail = formValue['usernameOrEmail'];
    const password = formValue['password'];
    this.subscription = this.userService.signInUser(usernameOrEmail, password).subscribe(
      response => {
             if(response.token && response.user) {
             const user= response.user ;
             const token= response.token;
             this.userService.saveUserData(user, token);
             this.router.navigate(['/dashbord']);
           } else {
             this.errorMessage = response.message;
           }
      },
      error => {
        this.errorMessage = error.error.message;
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
