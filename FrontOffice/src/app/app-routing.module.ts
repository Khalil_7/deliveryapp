import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { DashbordComponent } from './menu/dashbord/dashbord.component';
import { NotFoundComponent } from './menu/not-found/not-found.component';
import { OrdersComponent } from './menu/orders/orders.component';
import { StockComponent } from './menu/stock/stock.component';
import { AuthGuard } from './services/guards/auth.guard';


const routes: Routes = [
  { path: 'auth/signup' , component: SignupComponent },
  { path: 'auth/signin' , component: SigninComponent },
  { path: 'dashbord' , component: DashbordComponent , canActivate: [AuthGuard]},
  { path: 'stock' , component: StockComponent , canActivate: [AuthGuard]},
  { path: 'orders' , component: OrdersComponent , canActivate: [AuthGuard]},
  { path: 'not-found' , component: NotFoundComponent , canActivate: [AuthGuard]},
  { path: '', redirectTo: 'dashbord' , pathMatch: 'full'},
  { path: '**', redirectTo: 'not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
