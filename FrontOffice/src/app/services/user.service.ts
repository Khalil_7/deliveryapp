import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as AppUtile from '../common/app.util';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  URL = "http://127.0.0.1:3000/users";
  constructor(private http: HttpClient) { }

  signInUser(usernameOrEmail: string, password:string): Observable<any> {
    const path = "/signin";
    const userData = {usernameOrEmail: usernameOrEmail , password: password};
    return this.http.post<any>(this.URL + path , userData);
  }

  signUpUser(user): Observable<any>{
    const path = "/signup";
    return this.http.post(this.URL + path , user); 
  }

  signOutUser() {
    localStorage.clear();
  }

  saveUserData(user, token: string) {
    localStorage.setItem(AppUtile.AUTH_TOKEN, token);
    localStorage.setItem(AppUtile.USER_DATA, JSON.stringify(user));
  }

  getUserData() {
    const user = JSON.parse(localStorage.getItem(AppUtile.USER_DATA));
    const token = localStorage.getItem(AppUtile.AUTH_TOKEN);
    const userInfo = new User(
      user.fullname.toString(),
      user.username.toString(),
      user.email.toString(),
      user.role.toString(),
      '*********',
      user.isActive,
      user._id
      );
    return {userInfo: userInfo, token: token};
  }

  isLogedUser() : boolean {
    return !!localStorage.getItem(AppUtile.AUTH_TOKEN);
  }
    
}
