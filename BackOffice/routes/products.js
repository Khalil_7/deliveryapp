var express = require('express');
var router = express.Router();

const productController = require('../controllers/product');

router.get('/:clientId', productController.productsList);

router.get('/:clientId/:productId', productController.singleProduct);

router.post('/addproduct', productController.addProduct);

router.patch('/updateproduct/:productId', productController.updateProduct);

router.delete('/deleteproduct/:productId', productController.deleteProduct);

module.exports = router;