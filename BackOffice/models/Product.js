const mongoose = require('mongoose');

const productShema = mongoose.Schema({
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    qte: {
        type: Number,
        required: true,
        default: 0
    }
});

module.exports = mongoose.model('Product', productShema);