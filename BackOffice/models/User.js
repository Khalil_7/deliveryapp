const mongoose = require('mongoose');
const validator = require('validator');
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = mongoose.Schema({
    fullname: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: [true, "Username required"],
        unique: true
    },
    email: {
        type: String,
        lowercase: true,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: 'Email is not a valid email',
            isAsync: false
        },
        required: [true, "Email required"],
    },
    password: {
        type: String,
        required: [true, "Password required"],
    },
    role: {
        isClient: {
            type: Boolean,
            default: true
        },
        isDeliveryMan: {
            type: Boolean,
            default: false
        },
        isAdmin: {
            type: Boolean,
            default: false
        },
        isSuperAdmin: {
            type: Boolean,
            default: false
        },
    },
    isActive: {
        type: Boolean,
        default: false
    }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);