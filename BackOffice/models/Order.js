const mongoose = require('mongoose');
const Product = require('./Product');

const orderSchema = mongoose.Schema({
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }],
    products: [Product],
    adresse: {
        type: String,
        required: true
    },
    receiverName: {
        type: String
    },
    number: {
        type: String
    },
});

module.exports = mongoose.model('Order', orderSchema);