const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

exports.signup = (req, res, next) => {
    // Vérifier si le username ou l'email est déjà existé
    User.findOne({ $or: [{ 'username': req.body.username }, { 'email': req.body.email }] })
        .then(user => {
            if (user) {
                return res.status(400).json({ message: 'username ou email est déjà existé !' });
            }
            // Hacher le mot de passe
            bcrypt.hash(req.body.password, 10)
                .then(hash => {
                    const user = new User({
                        ...req.body,
                        password: hash
                    });
                    // Sauvegarder l'utilisateur
                    user.save()
                        .then(() => {
                            // on renvoie au frontend l'objet attendu
                            user.password = '********'; // pour ne pa envoyer le vrai mot de passe haché 
                            res.status(200).json({
                                user: user,
                                token: jwt.sign({ userId: user._id }, process.env.SERCRET_KEY, { expiresIn: '24h' })
                            });
                        })
                        .catch(error => res.status(400).json(error));
                })
                .catch(error => res.status(500).json(error));
        })
        .catch(error =>
            res.status(500).json(error)
        );
};

exports.signin = (req, res, next) => {
    User.findOne({ $or: [{ 'username': req.body.usernameOrEmail }, { 'email': req.body.usernameOrEmail }] })
        .then(user => {
            if (!user) {
                return res.status(401).json({ message: 'username invalide !' });
            }
            const password = req.body.password;
            const hash = user.password;
            // comparer le hach entré avec le hach dans la collection user 
            bcrypt.compare(password, hash)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ message: 'mot de passe invalide !' });
                    }
                    // on renvoie au frontend l'objet attendu
                    user.password = '********'; // pour ne pa envoyer le vrai mot de passe haché 
                    res.status(200).json({
                        user: user,
                        token: jwt.sign({ userId: user._id }, process.env.SERCRET_KEY, { expiresIn: '24h' })
                    });
                })
                .catch(error => res.status(500).json(error));
        })
        .catch(error => res.status(500).json(error));
};