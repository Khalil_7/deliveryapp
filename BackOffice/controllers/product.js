const Product = require('../models/Product');

exports.productsList = (req, res, next) => {
    Product.find({ client: req.params.clientId })
        .select('_id name price qte')
        .then(results => {
            if (results) {
                res.status(200).json(results);
            } else {
                res.status(200).json({
                    message: 'no product found'
                });
            }
        })
        .catch(error => {
            res.status(404).json(error);
        })
};

exports.singleProduct = (req, res, next) => {
    //verifier l'id du produit est pour le bon client 
    Product.findOne({ _id: req.params.productId, client: req.params.clientId })
        .select('_id name price qte')
        .then(doc => {
            if (doc) {
                res.status(200).json(doc);
            } else {
                res.status(404).json({
                    message: 'product not found'
                });
            }
        })
        .catch(error => {
            res.status(500).json(error);
        });
};

exports.addProduct = (req, res, next) => {
    // verifier si le produit existe deja pour ce client
    Product.findOne({ client: req.body.clientId, name: req.body.name })
        .then(result => {
            if (!result) {
                const product = new Product({
                    client: req.body.clientId,
                    name: req.body.name,
                    price: req.body.price,
                    qte: req.body.qte
                });
                product.save()
                    .then(result => {
                        res.status(201).json({
                            message: 'product added success !'
                        });
                    })
                    .catch(error => {
                        res.status(404).json({
                            message: error
                        })
                    });
            } else {
                res.status(404).json({
                    message: 'this product name is already existe'
                });
            }
        })
        .catch(error => {
            res.status(404).json(error)
        });
};

exports.updateProduct = (req, res, next) => {
    // verifier si le nom du produit existe deja pour ce client
    Product.findOne({ client: req.body.clientId, name: req.body.name })
        .then(result => {
            if (!result) {
                Product.findOneAndUpdate({ _id: req.params.productId }, {
                        // les parametres a modifier 
                        name: req.body.name,
                        price: req.body.price,
                        qte: req.body.qte
                    })
                    .then(doc => {
                        if (doc) {
                            res.status(200).json({
                                message: 'update product success'
                            });
                        } else {
                            res.status(404).json({
                                message: 'product not found !'
                            });
                        }
                    })
                    .catch(error => {
                        res.status(500).json(error);
                    });
            } else {
                res.status(404).json({
                    message: 'le nom du produit est déjà existé pour vous'
                });
            }
        })
        .catch(error => {
            res.status(500).json(error)
        });
};

exports.deleteProduct = (req, res, next) => {
    //verifier l'id du produit est pour ce client
    Product.findOneAndDelete({ _id: req.params.productId, client: req.body.clientId })
        .then(result => {
            if (result) {
                res.status(200).json({
                    message: 'delete product success'
                });
            } else {
                res.status(404).json({
                    message: 'product not found !'
                });
            }
        })
        .catch(error => {
            res.status(500).json({
                error: error
            });
        });
};